#!/bin/bash

set -ex

json_archive="/tmp/json_archive"
for passing_builds in $(jq -r '.builds | .[]| select(.result == "pass") | "\(.download_url)images/\(.machine)"' tuxoe-build.json); do
  echo ${passing_builds}
  curl -sSL -o "${json_archive}" "${passing_builds}/?export=json"
  machine=$(echo ${passing_builds}| awk -F '/' '{print $NF}')
  mkdir -p images/${machine}
  pushd images/${machine}
  for file in $(jq -r '.[][] | select(.Url) | .Url' "${json_archive}"); do
    echo curl -sSOL "${file}"
    curl -sSOL "${file}"
  done
  popd
done

rm "${json_archive}"
