# rootfs-maker

This builds an OpenEmbedded root file system using the provided `repo` manifest
that will fetch the right versions of the set of layers for the specified
combination of distro and branch.

By default, it builds OE/LKFT's Sumo branch using the latest released mainline
kernel, using the following manifest:

  https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest.git

## Usage

A pipeline will build OE for the following machines:

* am57xx-evm
* beaglebone
* dragonboard-410c
* dragonboard-845c
* hikey
* intel-core2-32
* intel-corei7-64
* juno

The default image is `rpb-console-image-lkft`.

All build artifacts will be stored in `/data/<BUILD_DATE>/<MACHINE>/` once the
build succeeds. The builder needs to have access to this directory. N.B.: If
running under Docker, this better be a volume for permanent storage.

### Variables

The following variables change the behavior and end result of the build:

* `MANIFEST_URL`: Use this manifest instead of the default.
  Default: `https://gitlab.com/Linaro/lkft/lkft-manifest.git`.
* `MANIFEST_BRANCH`: Use this branch of the manifest. Default: `sumo`.
* `DISTRO`: Which OE distro to build. Default: `lkft`.
* `IMAGE`: Bitbake target to build. Can be an image or any other recognized
  package. Default: `rpb-console-image-lkft`.

Other important variables that can be defined:
* `DATE`: Set this as the build date, which will become the subdirectory where
  the binaries will be stored. Default: today's date.
* `KERNEL_RECIPE`: Kernel recipe to use (preferred provider in OE). For a root
  file system the kernel should not matter much. Default:
  `linux-generic-mainline`.
* `KERNEL_VERSION`: Version of the kernel recipe to use. The mainline and next
  recipes use `git`, while the `linux-generic-stable-rc` recipe has the
  following versions available: `4.4`, `4.9`, `4.14`, `4.19`, `5.4`, `5.10`,
  `5.11`. Default: `git`.
* `SRCREV_kernel`: Git revision of the given kernel to use. There is no default,
  so the `SRCREV` defined by the recipe will be used.
