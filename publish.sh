#!/bin/bash

# Save image and all related artifacts
if [ -f "${CI_PROJECT_DIR}/date.txt" ]; then
  rootfs_date="$(cat "${CI_PROJECT_DIR}/date.txt")"
else
  echo "No date.txt found. Using today's date."
  rootfs_date="$(date +%Y%m%d)"
fi

if [ -f "${CI_PROJECT_DIR}/pub-dest.txt" ]; then
  pub_dest="$(cat "${CI_PROJECT_DIR}/pub-dest.txt")"
fi

# Install awscli binaries
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip -q awscliv2.zip
./aws/install --update -i ~/bin/aws-cli -b ~/bin

# Building complete image
dest_dir="images/${rootfs_date}"
mkdir -p "${dest_dir}"
rsync -axv images/ "${dest_dir}"

# Copy to AWS S3 bucket
if [ -z "${pub_dest}" ]; then
  branch_dir=${MANIFEST_BRANCH/\//_}
  branch_dir=${branch_dir,,}
  s3_dest="s3://storage.lkft.org/rootfs/oe-${branch_dir}/${rootfs_date}"
else
  s3_dest="${pub_dest}"
fi
~/bin/aws s3 sync --acl public-read \
  "images/${rootfs_date}/" \
  "${s3_dest}/"
